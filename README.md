# UIkit Components

The UIkit components module provides additional components and functionality
to the UIkit base theme.

Some aspects of the frontend cannot be themed without going through the backend,
such as empty navbar links and Views style plugins.

## CONTENTS OF THIS FILE


- Introduction
- Requirements
- Installation
- Configuration
- Supported modules
- Maintainers


## INTRODUCTION


The UIkit Components module is a companion module for the UIkit base theme.

- For a full description of the module, visit the project page:
   [uikit components](https://drupal.org/project/uikit_components)

- To submit bug reports and feature suggestions, or to track changes:
   [Bug reports](https://drupal.org/project/issues/uikit_components)


## REQUIREMENTS


Menu (Drupal core).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).



## CONFIGURATION


- The module settings can be found at `admin/config/user-interface/uikit-components`.


## MAINTAINERS

- Richard C Buchanan, III - [Richard Buchanan](https://www.drupal.org/u/richard-buchanan)
- Mitsuko - [Mitsuko](https://www.drupal.org/u/mitsuko)
- Rajab Natshah - [Rajab Natshah](https://www.drupal.org/u/rajab-natshah)
